runIt = {}
var servicio = require('../services/servicio') 
var consulta = [];

runIt.renIndex = function(req, res, next) {
    muestroCobayos = servicio.cobayos();
    res.render('index', {title: 'PetLove', favoritos:servicio.favsFun()});
}

runIt.keepData = function(req, res, next){
    
    console.log('La consulta es: ');
    console.log(req.body);

    if(!req.body)
        return res.status(400).send('error de busqueda.')

    consulta = servicio.busqFiltro(req);
    
    console.log('Lo que traigo es...');
    console.log(consulta);

    res.render('filtrado', {consulta} );

    
    //res.send('consultando...')

}

runIt.checkFav =function(req, res, next){
    var fav = req.body

}

runIt.paginar = function(req,res,next){

    let numero = 0;
    if(!req.body && !req.params)
        return res.status(400).send('error de busqueda.')

    let consulta = servicio.busqFiltro(req);
    console.log("En controler se renderiza lo siguiente: ");
    console.log(consulta);

    //Del resultado de la consulta se divide por 3
    var cantPaginas = consulta.length/3;
    

    //Se redondea el resultado por si da un numero con coma.
    if(!Number.isInteger(cantPaginas)){
		paginas = Math.floor(cantPaginas + 1)
	}else{
		paginas = cantPaginas
    }
    
    var arrayPaginas = Array(paginas).fill().map((e,i)=>i+1);
   
    if(req.params.page){
    
        var numeroParams = req.params.page;
    
        if(numeroParams<=paginas && numeroParams>0){
		    numero= numeroParams - 1;
		    console.log(numero)
		}
    }

    var principioIntervalo = numero*3;
    var finIntervalo = principioIntervalo + 3;
    var cobayosPagina = consulta.slice(principioIntervalo,finIntervalo);

    let edad,color,pelo;

    if (req.body){
        color = req.body.color;
        pelo =  req.body.pelo;
        edad =  req.body.edad;
    }
    
    if(req.params){
        color = req.params.color;
        pelo = req.params.pelo;
        edad = req.params.edad;  
    }
        
    res.render('filtrado', {cobayos:cobayosPagina, paginas:arrayPaginas, color: color,pelo:pelo,edad:edad })
}

module.exports = runIt;