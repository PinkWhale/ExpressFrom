var express = require('express');
var router = express.Router();

var controlador = require('../controller/control');

//GET
router.get('/', controlador.renIndex);
router.get('/:color/:pelo/:edad/:page', controlador.paginar);

//POST
router.post('/', controlador.paginar);//keep
router.post('/favoritos',controlador.checkFav);

module.exports = router;
