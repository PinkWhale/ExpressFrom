runIt = {};

//creo el objeto con sus atributos
let cobayos = [
    {
        id:1,
        edad:"joven",
        color:"bicolor",
        pelo: "largo",
        imagen:"/images/roed1.jpg",
        fav:true,
    },
    {
        id:2,
        edad:"joven",
        color:"multicolor",
        pelo: "corto",
        imagen:"/images/roed2.jpg",
        fav:true,
    },
    {
        id:3,
        edad:"adulto",
        color:"bicolor",
        pelo: "corto",
        imagen:"/images/roed3.jpg",
        fav:true,
    },
    {
        id:4,
        edad:"cachorro",
        color:"bicolor",
        pelo: "corto",
        imagen:"/images/roed4.jpg",
        fav:false,
    },
    {
        id:5,
        edad:"joven",
        color:"monocolor",
        pelo: "corto",
        imagen:"/images/roed5.jpg",
        fav:false,
    },
    {
        id:6,
        edad:"joven",
        color:"monocolor",
        pelo: "largo",
        imagen:"/images/roed6.jpg",
        fav:false,
    },
    {
        id:7,
        edad:"adulto",
        color:"monocolor",
        pelo: "corto",
        imagen:"/images/roed7.jpg",
        fav:false,
    },
    {
        id:8,
        edad:"adulto",
        color:"multicolor",
        pelo: "largo",
        imagen:"/images/roed8.jpg",
        fav:false,
    },
    {
        id:9,
        edad:"cachorro",
        color:"multicolor",
        pelo: "corto",
        imagen:"/images/roed9.jpg",
        fav:false,
    },
    {
        id:10,
        edad:"joven",
        color:"monocolor",
        pelo: "calvo",
        imagen:"/images/roed10.jpg",
        fav:false,
    },
    {
        id:11,
        edad:"cachorro",
        color:"multicolor",
        pelo: "calvo",
        imagen:"/images/roed11.jpg",
        fav:false,
    },
    {
        id:12,
        edad:"joven",
        color:"bicolor",
        pelo: "calvo",
        imagen:"/images/roed12.jpg",
        fav:false,
    },
    {
        id:13,
        edad:"joven",
        color:"monocolor",
        pelo: "calvo",
        imagen:"/images/roed13.jpg",
        fav:false,
    },
    {
        id:14,
        edad:"joven",
        color:"monocolor",
        pelo: "largo",
        imagen:"/images/roed14.jpg",
        fav:false,
    },
    {
        id:15,
        edad:"joven",
        color:"bicolor",
        pelo: "largo",
        imagen:"/images/roed15.jpg",
        fav:false,
    },
    {
        id:16,
        edad:"joven",
        color:"bicolor",
        pelo: "largo",
        imagen:"/images/roed16.jpg",
        fav:true,
    },
    {
        id:17,
        edad:"joven",
        color:"multicolor",
        pelo: "largo",
        imagen:"/images/roed17.jpg",
        fav:false,
    },
    {
        id:18,
        edad:"joven",
        color:"bicolor",
        pelo: "largo",
        imagen:"/images/roed18.jpg",
        fav:false,
    },
    {
        id:19,
        edad:"joven",
        color:"bicolor",
        pelo: "largo",
        imagen:"/images/roed19.jpg",
        fav:false,
    },
    
];

arrayFavs=[1, 2, 3];


//funcion para procesar los datos ingresados en el form
function procesoDatos(old, hair, colour){
    arrayCoincidencias=[];

    //consulto si existe algun comodin
    if (old!='comodin'){
        console.log('la consulta NO es comodin.');

        for (let i = 0; i < cobayos.length; i++) {
            const element = cobayos[i];
            
            if (hair == element.pelo && colour == element.color && old == element.edad){
                console.log('Encontró resultado!');
                arrayCoincidencias.push(element);
            }else{
                console.log('No hay concidencia con los 3 parametros D:');
            }
        }

        if(Object.keys(arrayCoincidencias).length === 0 ){
            console.log('No hubo coincidencias en la busqueda.');
        }else{
            console.log('El array de resultados es...');
            console.log(arrayCoincidencias);
            return arrayCoincidencias; 
        }


    }else{
        console.log('La consulta es comodin.');
              
        //recorro el objeto y busco las coincidencias
        for (let i = 0; i < cobayos.length; i++) {
            const element = cobayos[i];
            // console.log(element); <---esto lo use para analizar cada elemeto
            if (hair == element.pelo && colour == element.color){
                console.log('Encontro un resultado!');
                arrayCoincidencias.push(element);
            }else{
                console.log('No hay concidencia :(');
            }
        }

        //consulto si el objeto de resultado esta vacio
        if(Object.keys(arrayCoincidencias).length === 0 ){
            console.log('No hubo coincidencias en la busqueda.');
        }else{
            console.log('El array de resultados es...');
            console.log(arrayCoincidencias);
            return arrayCoincidencias; 
        }
    }
}


//Capturo los datos del form y comienzo el proceso de filtrado del comodin
runIt.busqFiltro = function (req, res, next){
    
    console.log(req.body);
    console.log(req.params);
    let edad,color,pelo;

    if (req.body){
        color = req.body.color.toString();
        console.log(color);
        pelo =  req.body.pelo.toString();
        edad =  req.body.edad.toString();
    }
    
    // if(req.params){
    //     color = req.params.color.toString();
    //     pelo = req.params.pelo.toString();
    //     edad = req.params.edad.toString();  
    // }

    console.log('Procesando los siguientes datos.. ');
    console.log(edad, pelo, edad);

    return procesoDatos(edad, pelo, color);
    
}

runIt.cobayos = function(){
	return cobayos;
}

runIt.favsFun = function(){
	return arrayFavs;
}

module.exports = runIt;